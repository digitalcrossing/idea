# idea

## Elevator Pitch

### deutsch

Schnellstmögliche Überquerung zweier Fahrspuren einer Ampelkreuzung für Fußgänger und Radfahrer.

#### Problem

Für die Verkehrsteilnehmer Fußgänger und Radfahrer gibt es beim Linksabbiegen
an Ampeln häufig die Situation, dass es verschiedene Wege über eine Kreuzung
zum Ziel gibt. 

Man kann aber im Vorfeld nicht abschätzen, welches der schnellere
Weg sein wird, um bei Grünphasen das Ziel zu erreichen. 

#### Lösung

Jede Ampel arbeitet nach einem festgelegten Schaltplan (Signalzeitenplan), es wäre 
es ein leichtes, durch Transparenz diese Information dem Verkehrsteilnehmer
aufzuzeigen.

Benötigt wird eine Möglichkeit diese Information schnell und einfach
für eine Ampel, vor der man steht, abzufragen.


* Abfrage des Signalzeitenplans
* Zustand, in dem die Ampel sich gerade befindet
* Position des Verkehrsteilnehmers
* Ziel des Verkehrsteilnehmers

Aus diesen Informationen würde sich eine optimierte Navigation über die Kreuzung ergeben.

#### Quellen, weitergehende Informationen

* Ampel https://de.wikipedia.org/wiki/Ampel
* Technische Details der Steuerung https://de.wikipedia.org/wiki/Ampel#Ablauf_und_Technik
* Signalzeitenplan https://de.wikipedia.org/wiki/Signalzeitenplan
* Deutsche Richtlienien für LSA https://de.wikipedia.org/wiki/Richtlinien_f%C3%BCr_Lichtsignalanlagen

